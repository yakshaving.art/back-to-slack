package main

import (
	"fmt"
	"log"

	"github.com/xanzy/go-gitlab"
)

var ServiceNotFoundErr = fmt.Errorf("service not found")

// GitlabService knows how to communicate with Gitlab API.
// This interface aim isn't to abstract github.com/xanzy/go-gitlab library just have
// different implementations around that library.
type GitlabService interface {
	ListProjects() ([]*gitlab.Project, error)
	GetSlackService(projectID int) (*gitlab.SlackService, error)
	SetSlackService(projectID int, opt *gitlab.SetSlackServiceOptions) error
}

type gitlabService struct {
	client *gitlab.Client
}

// NewGitlabService returns a GitlabService that uses gitlab.Client
// to communicate with Gitlab API.
func NewGitlabService(c *gitlab.Client, dryRun bool) GitlabService {
	svc := gitlabService{client: c}
	if dryRun {
		return gitlabDryRunService{gitlabService: svc}
	}

	return svc
}

func (g gitlabService) ListProjects() ([]*gitlab.Project, error) {
	var (
		trueBool              = true
		maintainerPermissions = gitlab.MaintainerPermissions
	)

	projects := []*gitlab.Project{}
	page := 1
	for {
		opts := gitlab.ListProjectsOptions{
			Membership:     &trueBool,
			MinAccessLevel: &maintainerPermissions,
			ListOptions: gitlab.ListOptions{
				PerPage: 100,
				Page:    page,
			},
		}
		prjs, _, err := g.client.Projects.ListProjects(&opts)
		if err != nil {
			return nil, err
		}
		projects = append(projects, prjs...)

		if len(prjs) < 100 {
			break
		}
		page++
	}

	return projects, nil
}

func (g gitlabService) GetSlackService(projectID int) (*gitlab.SlackService, error) {
	svc, resp, err := g.client.Services.GetSlackService(projectID)
	if err != nil && resp.StatusCode == 404 {
		return nil, ServiceNotFoundErr
	}
	return svc, err
}

func (g gitlabService) SetSlackService(projectID int, opt *gitlab.SetSlackServiceOptions) error {
	_, err := g.client.Services.SetSlackService(projectID, opt)
	return err
}

type gitlabDryRunService struct {
	gitlabService
}

func (g gitlabDryRunService) SetSlackService(projectID int, opt *gitlab.SetSlackServiceOptions) error {
	log.Printf("[DRYRUN] skipping slack integration configuration for project ID: %d\n", projectID)
	return nil
}
