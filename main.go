package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"regexp"

	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

var (
	debugMode = false
)

// Config configuration
type Config struct {
	Projects map[string]IntegrationConfig `yaml:"projects"`
}

// IntegrationConfig represents the configutaion of a single integration
type IntegrationConfig struct {
	WebhookVar                string `yaml:"webhook"`
	Username                  string `yaml:"username"`
	Channel                   string `yaml:"channel"`
	BranchesToBeNotified      string `yaml:"branches_to_be_notified"`
	NotifyOnlyBrokenPipelines bool   `yaml:"notify_only_broken_pipelines"`
	NotifyAllEvents           bool   `yaml:"notify_all_events"`
	ConfidentialIssueChannel  string `yaml:"confidential_issue_channel"`
	ConfidentialIssuesEvents  bool   `yaml:"confidential_issues_events"`
	// TODO: Currently, GitLab ignores this option (not implemented yet?), so
	// there is no way to set it. Uncomment when this is fixed.
	// See: https://gitlab.com/gitlab-org/gitlab/-/issues/23479
	//ConfidentialNoteChannel   string `yaml:"confidential_note_channel"`
	ConfidentialNoteEvents bool   `yaml:"confidential_note_events"`
	DeploymentChannel      string `yaml:"deployment_channel"`
	DeploymentEvents       bool   `yaml:"deployment_events"`
	IssueChannel           string `yaml:"issue_channel"`
	IssuesEvents           bool   `yaml:"issues_events"`
	MergeRequestChannel    string `yaml:"merge_request_channel"`
	MergeRequestsEvents    bool   `yaml:"merge_requests_events"`
	NoteChannel            string `yaml:"note_channel"`
	NoteEvents             bool   `yaml:"note_events"`
	PipelineChannel        string `yaml:"pipeline_channel"`
	PipelineEvents         bool   `yaml:"pipeline_events"`
	PushChannel            string `yaml:"push_channel"`
	PushEvents             bool   `yaml:"push_events"`
	TagPushChannel         string `yaml:"tag_push_channel"`
	TagPushEvents          bool   `yaml:"tag_push_events"`
	WikiPageChannel        string `yaml:"wiki_page_channel"`
	WikiPageEvents         bool   `yaml:"wiki_page_events"`
}

func main() {
	log.SetFlags(0)
	dryRun := false

	force := flag.Bool("force", false, "force applying the configuration even if it's already there")
	configFile := flag.String("config", "config.yml", "set the configuration file")

	flag.BoolVar(&debugMode, "debug", false, "enable debug mode")
	flag.BoolVar(&dryRun, "dry-run", false, "enable dry-run mode")

	flag.Parse()

	token := MustGetEnv("GITLAB_TOKEN")
	gitlabBaseURL := os.Getenv("GITLAB_BASEURL")

	b, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalf("failed to read config file %q: %q", *configFile, err)
	}

	conf := Config{}
	err = yaml.UnmarshalStrict(b, &conf)
	if err != nil {
		log.Fatalf("failed to parse config file: %q", err)
	}

	if gitlabBaseURL == "" {
		gitlabBaseURL = "https://gitlab.com/"
	}
	cli, err := gitlab.NewClient(token, gitlab.WithBaseURL(gitlabBaseURL))
	if err != nil {
		log.Fatalf("failed to create gitlab client: %q", err)
	}

	glSvc := NewGitlabService(cli, dryRun)

	c := Configurator{
		Config:        conf,
		GitlabService: glSvc,
		Force:         *force,
	}
	if err := c.Configure(); err != nil {
		log.Fatalf("failed to configure stuff: %s", err)
	}
}

// Configurator configures stuff
type Configurator struct {
	Config        Config
	GitlabService GitlabService
	Force         bool
}

// Configure configures stuff
func (c Configurator) Configure() error {
	projects, err := c.GitlabService.ListProjects()
	if err != nil {
		return fmt.Errorf("could not list Gitlab projects: %q", err)
	}

	var lastError error
	for _, p := range projects {
		if err := c.Apply(p); err != nil {
			log.Printf("failed to apply project %q: %q", p.PathWithNamespace, err)
			lastError = err
		}
	}

	return lastError
}

// Apply applies stuff
func (c Configurator) Apply(p *gitlab.Project) error {
	for pattern, integration := range c.Config.Projects {
		m, err := regexp.MatchString(pattern, p.PathWithNamespace)
		if err != nil {
			return err
		}
		if !m {
			debug("%q does not match the configuration, ignoring...", p.PathWithNamespace)
			continue
		}

		debug("project %q matched", p.PathWithNamespace)

		// If is URL then don't get from environment.
		slackHook := integration.WebhookVar
		if !isURL(slackHook) {
			slackHook = MustGetEnv(integration.WebhookVar)
		}

		var branches *string
		if integration.BranchesToBeNotified != "" {
			branches = &integration.BranchesToBeNotified
		}

		var username *string
		if integration.Username != "" {
			username = &integration.Username
		}

		opt := gitlab.SetSlackServiceOptions{
			WebHook:                   &slackHook,
			Username:                  username,
			NotifyOnlyBrokenPipelines: &integration.NotifyOnlyBrokenPipelines,
			BranchesToBeNotified:      branches,
			ConfidentialNoteEvents:    &integration.ConfidentialNoteEvents,
			ConfidentialIssueChannel:  integration.channelOrDefault(integration.ConfidentialIssueChannel),
			ConfidentialIssuesEvents:  integration.activeOrDefault(integration.ConfidentialIssuesEvents),
			DeploymentChannel:         integration.channelOrDefault(integration.DeploymentChannel),
			DeploymentEvents:          integration.activeOrDefault(integration.DeploymentEvents),
			IssueChannel:              integration.channelOrDefault(integration.IssueChannel),
			IssuesEvents:              integration.activeOrDefault(integration.IssuesEvents),
			MergeRequestChannel:       integration.channelOrDefault(integration.MergeRequestChannel),
			MergeRequestsEvents:       integration.activeOrDefault(integration.MergeRequestsEvents),
			NoteChannel:               integration.channelOrDefault(integration.NoteChannel),
			NoteEvents:                integration.activeOrDefault(integration.NoteEvents),
			PipelineChannel:           integration.channelOrDefault(integration.PipelineChannel),
			PipelineEvents:            integration.activeOrDefault(integration.PipelineEvents),
			PushChannel:               integration.channelOrDefault(integration.PushChannel),
			PushEvents:                integration.activeOrDefault(integration.PushEvents),
			TagPushChannel:            integration.channelOrDefault(integration.TagPushChannel),
			TagPushEvents:             integration.activeOrDefault(integration.TagPushEvents),
			WikiPageChannel:           integration.channelOrDefault(integration.WikiPageChannel),
			WikiPageEvents:            integration.activeOrDefault(integration.WikiPageEvents),
		}

		serv, err := c.GitlabService.GetSlackService(p.ID)
		if err == ServiceNotFoundErr {
			// Not found, just set it up
			err = c.GitlabService.SetSlackService(p.ID, &opt)
			if err != nil {
				return fmt.Errorf("failed to set slack configuration in Gitlab for project %q: %q", p.PathWithNamespace, err)
			}
			log.Println(p.PathWithNamespace, "slack integration is now configured on Gitlab")
			continue

		} else if err != nil {
			return fmt.Errorf("failed to get slack configuration from Gitlab for project %q: %q", p.PathWithNamespace, err)
		}

		if serv.Active {
			if !c.Force && !configChanged(opt, serv) {
				log.Println(p.PathWithNamespace, "service is active but slack integration is correctly configured already")
				return nil
			}
			debug("  service is active on the Gitlab side, but the configuration seems to have changed")
		}

		err = c.GitlabService.SetSlackService(p.ID, &opt)
		if err != nil {
			return fmt.Errorf("failed to set slack configuration in Gitlab for project %q: %q", p.PathWithNamespace, err)
		}
		log.Println(p.PathWithNamespace, "slack integration is now configured on Gitlab")
	}

	return nil
}

// MustGetEnv either gets the environment variable, or crashes the app
func MustGetEnv(key string) string {
	value := os.Getenv(key)
	if value == "" {
		log.Fatalf("Environment variable %q is mandatory", key)
	}
	return value
}

func (i IntegrationConfig) channelOrDefault(config string) *string {
	if config != "" {
		return &config
	}
	return &i.Channel
}

func (i IntegrationConfig) activeOrDefault(config bool) *bool {
	if config {
		return &config
	}
	return &i.NotifyAllEvents
}

func configChanged(a gitlab.SetSlackServiceOptions, b *gitlab.SlackService) bool {
	if b == nil {
		debug("  project has no service properties")
		return true
	}

	changedString := func(name, current string, proposed *string) func() bool {
		return func() bool {

			if proposed == nil {
				debug("  %s not defined as a proposed value", name)
				return false
			}

			if *proposed != current {
				debug("  %s changed from %q to %q", name, current, *proposed)
				return true
			}
			debug("  %s remains unchanged: %q", name, current)
			return false
		}
	}
	changedBool := func(name string, current bool, proposed *bool) func() bool {
		return func() bool {
			if proposed == nil {
				debug("  %s not defined as a proposed value", name)
				return false
			}

			if *proposed != current {
				debug("  %s changed from %t to %t", name, current, *proposed)
				return true
			}
			debug("  %s remains unchanged: %t", name, current)
			return false
		}
	}

	checks := []func() bool{
		changedString("Webhook", b.Properties.WebHook, a.WebHook),

		changedBool("PushEvents", b.PushEvents, a.PushEvents),
		changedBool("IssuesEvents", b.IssuesEvents, a.IssuesEvents),
		changedBool("NoteEvents", b.NoteEvents, a.NoteEvents),
		changedBool("MergeRequestsEvents", b.MergeRequestsEvents, a.MergeRequestsEvents),
		changedBool("ConfidentialIssuesEvents", b.ConfidentialIssuesEvents, a.ConfidentialIssuesEvents),
		changedBool("ConfidentialNoteEvents", b.ConfidentialNoteEvents, a.ConfidentialNoteEvents),
		changedBool("PipelineEvents", b.PipelineEvents, a.PipelineEvents),
		changedBool("TagPushEvents", b.TagPushEvents, a.TagPushEvents),
		changedBool("WikiPageEvents", b.WikiPageEvents, a.WikiPageEvents),

		changedString("DeploymentChannel", b.Properties.DeploymentChannel, a.DeploymentChannel),
		changedString("BranchesToBeNotified", b.Properties.BranchesToBeNotified, a.BranchesToBeNotified),
		changedString("ConfidentialIssueChannel", b.Properties.ConfidentialIssueChannel, a.ConfidentialIssueChannel),
		changedString("DeploymentChannel", b.Properties.DeploymentChannel, a.DeploymentChannel),
		changedString("IssueChannel", b.Properties.IssueChannel, a.IssueChannel),
		changedString("MergeRequestChannel", b.Properties.MergeRequestChannel, a.MergeRequestChannel),
		changedString("NoteChannel", b.Properties.NoteChannel, a.NoteChannel),
		changedBool("NotifyOnlyBrokenPipelines", bool(b.Properties.NotifyOnlyBrokenPipelines), a.NotifyOnlyBrokenPipelines),
		changedBool("NotifyOnlyDefaultBranch", bool(b.Properties.NotifyOnlyDefaultBranch), a.NotifyOnlyDefaultBranch),
		changedString("PipelineChannel", b.Properties.PipelineChannel, a.PipelineChannel),
		changedString("PushChannel", b.Properties.PushChannel, a.PushChannel),
		changedString("TagPushChannel", b.Properties.TagPushChannel, a.TagPushChannel),
		changedString("WikiPageChannel", b.Properties.WikiPageChannel, a.WikiPageChannel),

		// Not supported by the API
		// changedBool("", b.CommentOnEventEnabled, a.CommentOnEventEnabled),
		// changedBool("", b.CommitEvents, a.CommitEvents),
		// changedBool("", b.JobEvents, a.JobEvents),
		// changedString("ConfidentialNoteChannel", b.Properties.ConfidentialNoteChannel, a.ConfidentialNoteChannel),
	}

	for _, c := range checks {
		if c() {
			return true
		}
	}

	return false
}

func debug(format string, v ...interface{}) {
	if debugMode {
		log.Printf(format, v...)
	}
}

func isURL(u string) bool {
	_, err := url.ParseRequestURI(u)
	if err != nil {
		return false
	}
	return true
}
