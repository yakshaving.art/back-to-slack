# Back to slack

Configures the slack notification integration for all the projects that match.

## Usage

`./back-to-slack [-force]`

## Environment variables

### GITLAB_TOKEN

The token used to talk to gitlab API, required

### GITLAB_BASEURL

The url in which gitlab API is, by default gitlab.com

## How to configure

Examples:

```yml
---
projects:
  group/repo.*:
    webhook: ENV_VAR_FOR_SLACK_URL # If the webhook URL is set it will not be search as an env var, otherways yes.
    channel: "#the_channel" # optional
    branches_to_be_notified: default # by default, valid values are “all”,
                             # “default”, “protected”, and “default_and_protected”
    notify_only_broken_pipelines: false

    # the following configs are all optional
    username: "Gitlab Reporter"
    notify_only_broken_pipelines: false
    confidential_issue_channel: "#the_channel_1"
    confidential_issues_events: true
    confidential_note_channel: "#the_channel_2"
    confidential_note_events: true
    deployment_channel: "#the_channel_3"
    deployment_events: true
    issue_channel: "#the_channel_4"
    issues_events: true
    merge_request_channel: "#the_channel_5"
    merge_requests_events: true
    note_channel: "#the_channel_6"
    note_events: true
    pipeline_channel: "#the_channel_7"
    pipeline_events: true
    push_channel: "#the_channel_8"
    push_events: true
    tag_push_channel: "#the_channel_9"
    tag_push_events: true
    wiki_page_channel: "#the_channel_10"
    wiki_page_events: true
```

```yml
---
projects:
  teamA/appA/api-service:
    webhook: https://hooks.slack.com/services/XXXXXXX/YYYYYYYYY/ZZZZZZZZZZZZZZZZZZz
    branches_to_be_notified: all
    notify_only_broken_pipelines: true
```
