module gitlab.com/yakshaving.art/back-to-slack

go 1.16

require (
	github.com/xanzy/go-gitlab v0.50.0
	gopkg.in/yaml.v2 v2.4.0
)
